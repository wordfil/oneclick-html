<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>VisaPRO - Immigration and Visa Consulting Website Template</title>

<link href="assets/compress.css" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">



</head>
<header id="sdfsdfdsfheader-2" class="header white-menu">

    <div class="header-wrapper">

        <div class="rolling-txt">
            <div class="rolling-news">Latest News</div><marquee onmouseover="this.stop();" onmouseout="this.start();" class="rolling-box" style="height: 38px;" scrollamount="4" ;="">
                <p><a href="">Canada welcomes 4,200 skilled workers under new FSWP Express Entry Draw</a> |
                    <a href="">Canada PNP 2020: 5 Easy Steps Canada Provincial Nominee Program</a> |
                    <a href="">Two Ways to Immigrate to Canada in 2020</a> |
                    <a href="">Minimum Income to Sponsor Parents in Canada 2020</a> |
                    <a href="https://oneclickvisas.com/immigration-news/six-reasons-why-your-canadian-pr-visa-may-get-denied/">Six Reasons Why your Canadian Permanent Resident Visa may get denied</a> |  </p>
            </marquee>
        </div>

        <div class="wsmobileheader clearfix">
            <span class="menu-text hidden-md hidden-lg hidden-sm">MENU</span>
            <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
            <span class="smllogo"><img src="assets/images/logo.svg" width="250px" alt="mobile-logo"/></span>
            <a href="tel:123456789" class="callusbtn"><i class="fas fa-phone"></i></a>
        </div>


        <!-- HEADER STRIP -->
       


        <!-- NAVIGATION MENU -->
        <div class="wsmainfull menu clearfix">
            <div class="wsmainwp clearfix" style="padding: 0 0px!important;">


                <!-- LOGO IMAGE -->
                <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 360 x 90 pixels) -->
                <div class="desktoplogo"><a href="#hero-3" class="logo-black"><img src="assets/images/logo.svg" width="250px" alt="header-logo"></a></div>
                <div class="desktoplogo"><a href="#hero-3" class="logo-white"><img src="assets/images/logo.svg" width="250px"  alt="header-logo"></a></div>


                <!-- MAIN MENU -->
                <nav class="wsmenu clearfix blue-header">
                    <ul class="wsmenu-list">


                        <li><a href="">Home</a></li>
                        <li aria-haspopup="true"><a href="#">About <span class="wsarrow" style="float: right;
                            margin-left: 42px;"></span></a>
                            <ul class="sub-menu">
                                <li aria-haspopup="true"><a href="#">Why One Click Visas</a></li>
                                <li aria-haspopup="true"><a href="#">Dummy Link #2</a></li>
                                <li aria-haspopup="true"><a href="#">Dummy Link #3</a></li>
                                <li aria-haspopup="true"><a href="#">Dummy Link #4</a></li>
                                <li aria-haspopup="true"><a href="#">Dummy Link #5</a></li>
                            </ul>
                        </li>	<!-- END DROPDOWN MENU -->


                        <!-- PAGES -->
                        <li aria-haspopup="true"><a href="#">Service <span class="wsarrow" style="float: right;
                            margin-left: 42px;"></span></a>
                            <div class="wsmegamenu clearfix">
                                <div class="container">
                                    <div class="row mega-menu-background">




                                        <section id="what-we-do">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="assets/images/flags/canada.png" width="50px" alt="header-logo">Permanent Residency in Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="assets/images/flags/canada.png" width="50px" alt="header-logo">Study in Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="assets/images/flags/canada.png" width="50px" alt="header-logo">Work Permit For Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="assets/images/flags/canada.png" width="50px" alt="header-logo">Tourist Visa for Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="assets/images/flags/canada.png" width="50px" alt="header-logo">Open Work Permit in Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="assets/images/flags/canada.png" width="50px" alt="header-logo">Spouse Visa for Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>




                                    </div>  <!-- End row -->
                                </div>  <!-- End container -->
                            </div>  <!-- End wsmegamenu -->
                        </li>	<!-- END PAGES -->
                        <li><a href="">News</a></li>
                        <li><a href="">Contact</a></li>
                        <li><a href="">Pay-now</a></li>
                        <li class="nl-simple" aria-haspopup="true">
                            <a href="#" class="header-btn btn-primary tra-black-hover last-link">FREE CONSULTANTS</a>
                        </li>
                        <li><img src="assets/images/IRC.png" class="irc"></li>


                    </ul>
                </nav>	<!-- END MAIN MENU -->

            </div>
        </div>	<!-- END NAVIGATION MENU -->


    </div>     <!-- End header-wrapper -->
</header>	<!-- END HEADER -->




<div id="page" class="page">
    <section id="hero-3" class="hero-section division">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-7 col-xl-7">
                    <div class="hero-3-txt mb-40 font-weight700">

                        <h1 class="darkblue-color">Professional Assistance of SoftTrench Services</h1>
                        <div class="box-list">
                            <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                            <ul class="listing">
                                <li>
                                    <p class="p-md">
                                        Welcome to Oneclick Visas! We offer a large scope of immigration services and visa help to our customers willing to fly & settle abroad.
                                    </p>
                                </li>
                                <li>
                                    <p class="p-md">
                                        We keep reshaping ourselves, expanding our range of services to students, tourist and job seekers. Our world-class quality of immigration assistance makes us the #1 immigration consultancy in India!
                                    </p>
                                </li>
                                <li>
                                    <p class="p-md">
                                        Take your first step towards your dream!- Check your eligibility today!!
                                    </p>
                                </li>
                            </ul>

                        </div>
                        <div class="box-list">
                            <div class="box-list-icon"><i class="fas fa-genderless"></i></div>

                        </div>

                    </div>
                </div>
                <div class="col-lg-5 col-xl-5 xsnospace">
                    <div id="hero-form" class="text-center mb-40">
                       <?php include('include/form.php'); ?>
                    </div>
                </div>


            </div>
        </div>
    </section>


    <section id="services-4" class="bg-lightgrey services-section division">
        <div class="container">
            <div class="container">
            <p style="height:30px;"></p>
            <div class="row ndrowdata operation-ata">
                <h3 class="ol-md-12 col-lg-12 h3-lg gray-color text-center">Where would you like to go? </h3>
                <center class="col-md-12"><span class="line"></span></center>
                <div class="col-md-6 col-lg-6">
                    <h5 class="h5-md canada-flag" style="background: url(assets/images/can-flag-1.jpg) no-repeat right"><b>Canada PR Visa</b></h5>
                    <h6 class="h6-md">Popular Topics in Canadian Immigration</h6>
                    <ul>
                        <li><a href="">Canada PR Visa</a></li>
                        <li><a href="">67 Points Calculator</a></li>
                        <li><a href="">Canada Express Entry Program</a></li>
                        <li><a href="">Canada Provincial Nominee Program</a></li>
                        <li><a href="">PR Visa Fees for Canada</a></li>
                        <li><a href="">Saskatchewan Immigration Nominee Program(SINP)</a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-6">
                    <h5 class="h5-md australia-flag" style="background: url(assets/images/aus-flag-1.jpg) no-repeat right"><b>Australia PR Visa</b></h5>
                    <h6 class="h6-md">Popular Topics in Australia Immigration</h6>
                    <ul>
                        <li><a href="">Points Calculator For Australia PR</a></li>
                        <li><a href="">Australia PR Visa</a></li>
                        <li><a href="">189 Visa 2019</a></li>
                        <li><a href="">Australia PR Process Steps</a></li>
                        <li><a href="">PR Visa Fees for Australia</a></li>
                        <li><a href="">New 491 Visa</a></li>
                    </ul>
                </div>
            </div>
            </div>
        </div>
    </section>

    <?php include('include/check.php'); ?>

    <div class="section-divider"><div class="container"><div class="row"><div class="grey-border"></div></div></div></div>

    <section id="about-2" class="about-section division" style="background:url(assets/images/about-8.jpg); background-size:cover; ">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-6">
                    <div class="about-img text-center">
                        <div class="section25">
                            <h3>Certified Canadian Immigration Consultant in India !</h3>
                            <br>
                            <p>
                                Oneclick Visas has been into consulting services in the field of immigration since a decade now. Our Canada Branch office has a complete backing for us so that we can comfortably work in India and make your Immigration dream a success! We are a group of experienced visa consultants and we work for you through day and night to help in your visa achievement.
                            </p><br>
                            <p>
                                We are an ICCRC accredited firm and we are glad to provide the most authentic immigration services in India.
                            </p><br>
                            <p >
                                At Oneclick Visas, your PR is guaranteed within 6 months of time period! Get in touch with us now and let us help you get your visa application placed
                            </p><br>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                </div>


            </div>
        </div>
    </section>

    <section id="about-4" class="bg-lightgrey bg-tra-city about-section division">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-7 col-xl-6">
                    <div class="about-4-txt mb-40">

                        <br><br>
                        <span class="section-id id-color professional-advisor">Professional Advisors</span>

                        <!-- Title -->
                        <h3 class="h3-lg darkblue-color">We Provide the best consulting Services in the industry since 2011! </h3>

                        <ul class="profess-data">
                            <li>
                                <span>What makes us stand apart from other experts is that we are a genuine, authorised immigration consultancy with a record success of about 6000+ PR cases filed and achieved! Also, we are extremely grateful to Mr.Angad Sachdeva (Certified Immigration Consultant certified and courtesy RCIC Member) for being on board with us!</span>
                            </li>
                            <li>
                                <span>Mr.Sachdeva, apart from being the versatile immigration consultant, has reasonable immigration knowledge. He is well-versed with the existing immigration laws as well as the potential policy changes that are likely to take place in the world of immigration due to changing diplomatic ties of India.</span>
                            </li>
                            <li>
                                <span>Oneclick Visas, under the true guidance of Mr.Sachdeva, is a result-centered immigration firm proving visa assistance for different developed nations. With satisfactory client support system, we offer 24 hours online help. We employ a committed group of exceptionally talented, learned and experienced specialists who attend every query of our regarded customers.</span>
                            </li>
                        </ul>

                        <a href="tel:123456789" class="btn btn-md btn-primary black-hover">
                            Call: 0844-844-1085
                        </a>
                        <br>

                    </div>
                </div>
                <div class="col-lg-5 col-xl-5 offset-xl-1">
                    <div class="about-4-img text-center" style="padding:20px; box-shadow: 0px 0px 7px #cccccc;">
                        <img class="img-fluid" src="assets/images/image-04.jpg" alt="about-image" />
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section id="about-4" class="bg-lightgrey bg-tra-city about-section division">
        <div class="container">
            <br>
            <?php include('include/why.php'); ?>
        </div>
    </section>

    <div id="statistic-2" class="bg-blue-map wide-30 statistic-section division">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title white-color center">
                    <h2 class="h2-xs 1darkblue-color">Why Choose ONECLICK VISAS?</h2>
                    <p class="p-md">BEST CANADA IMMIGRATION CONSULTANT IN INDIA</p>
                </div>
                <div class="widthfull"><div class="width300px"><span class="line1 line-white"></span><span class="logo-ii"><img src="assets/images/NewProject.png" class="lead-img"></span><span class="line12 line-white"></span></div></div>
                <p class="text-center text-white">Oneclick Visas is India’s No.1 Canada Immigration Consultant Company in Delhi, which has its head office located at Montreal, Canada. Oneclick Visas is the most experienced and trusted visa advisor in Delhi, India to meet all your immigration needs.
                </p>
                <p class="text-center text-white">Oneclick Visas deal with your visa application process and provide all the essential help in getting your PR visa approved without any hassle. We adopt a customer-friendly way of dealing. We have an understanding of immigration experience crosswise various visa types and we make use of our insights, skills and years of experience to make your procedure relaxed and quick.
                </p>
            </div>

            <div class="row strongsetting">
                <div class="col-md-6 col-lg-3">
                    <div style="width:100%; height:120px; background:url(assets/images/student.jpg);"></div>
                    <div class="statistic-block icon-sm">
                        <span class="flaticon-316-mortarboard darkblue-color"></span>
                        <h5 class="primary-color" style="margin: auto !important; display: table;">1220+</h5>
                        <p class="p-md darkblue-color" style="margin: auto !important; display: table;">Students</p>
                    </div>
                </div>


                <div class="col-md-6 col-lg-3">
                    <div style="width:100%; height:120px; background:url(assets/images/university.jpg);"></div>
                    <div class="statistic-block icon-sm">
                        <span class="flaticon-431-bank darkblue-color"></span>
                        <h5 class="primary-color" style="margin: auto !important; display: table;">315</h5>
                        <p class="p-md darkblue-color" style="margin: auto !important; display: table;">Universities</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div style="width:100%; height:120px; background:url(assets/images/country.jpg);"></div>
                    <div class="statistic-block icon-sm">
                        <span class="flaticon-285-internet-2 darkblue-color"></span>
                        <h5 class="primary-color" style="margin: auto !important; display: table;">74</h5>
                        <p class="p-md darkblue-color" style="margin: auto !important; display: table;">Countries</p>
                    </div>
                </div>


                <div class="col-md-6 col-lg-3">
                    <div style="width:100%; height:120px; background:url(assets/images/immigration.jpg);"></div>
                    <div class="statistic-block icon-sm">
                        <span class="flaticon-067-suitcase-1 darkblue-color"></span>
                        <h5 class="primary-color" style="margin: auto !important; display: table;">2301 +</h5>
                        <p class="p-md darkblue-color" style="margin: auto !important; display: table;">Immigrations</p>
                    </div>
                </div>

                <p class="text-center text-white">
                    As a Registered Migration Agents, our specialists hold fast to the guidelines set out by the ICCRC in its Code of Conduct. We are thus an certified and authorized Canadian Consultants in Delhi for Canada Immigration.
                </p>


            </div>
        </div>
    </div>

    <section id="tabs-1" class="bg-lightgrey wide-100 tabs-section division">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title center">
                    <h2 class="h2-xs darkblue-color">Our Accreditation </h2><br>
                    <h5 class="h5-md grey-color">We partner with companies of all sizes, all around the world</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <?php include('include/agre.php'); ?>
                </div>
            </div>
        </div>
    </section>

    <section id="reviews-1" class="bg-lightgrey wide-100 tabs-section division" style="background:url(assets/images/wall.jpg); background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title center">
                    <h2 class="h2-xs darkblue-color">What Our Clients Say</h2>
                    <p class="p-md">Cursus porta, feugiat primis in ultrice ligula risus auctor tempus dolor feugiat,
                        felis lacinia risus interdum auctor id viverra dolor iaculis luctus placerat and massa
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <?php include('include/says.php'); ?>
                </div>
            </div>
        </div>
    </section>

    <section id="reviews-1" class="bg-lightgrey tabs-section division" style="background:url(assets/images/blue-back.svg); background-size: cover;">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-1 col-xl-1"></div>

                <div class="col-lg-6 col-xl-6">
                    <div class="hero-3-txt mb-40">


                        <div class="col-lg-12 col-md-12 col-sm-12 content-column">
                            <div class="content-box">
                                <br><br><h3>Customer Ratings</h3>
                                <ul class="rating-d clearfix staRTSIZE">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                <h2>4.8 / 5.0</h2>
                                <span>By 1500+ Visa Approved Customers</span>
                                <p style="height:20px"></p>

                                <ul class="info-box clearfix">
                                    <li>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <p>Any Questions? Call us</p>
                                        <h3><a href="tel:12463330079" target="_blank" rel="nofollow">+1 (246) 333 0079</a></h3>
                                    </li> 								 <li>
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <p>Any Questions? Email us</p>
                                        <h3><a href="mailto:inquiry@example.com" target="_blank" rel="nofollow">inquiry@example.com</a></h3>
                                    </li>
                                </ul><br>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-lg-5 col-xl-5">
                    <div id="hero-form" class="text-center mb-40">
                       <?php include('include/form.php'); ?>
                    </div>
                </div>


            </div>
        </div>
    </section>

    <section id="reviews-1" class="bg-lightgrey wide-100 tabs-section division" style="background:white;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-title center">
                    <h2 class="h2-xs darkblue-color">Our Stories & Latest News   </h2>
                    <p class="p-md">Know latest changes in the immigration scenario around the globe!           </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <?php include('include/says.php'); ?>
                </div>
            </div>
        </div>
    </section>



</div>

<section id="reviews-1" class="bg-lightgrey tabs-section division" style="background:url(assets/images/wall.jpg); background-size: cover;">
    <br><br> <div class="container">
            <div class="row">
                <div class="col-md-12 section-title center">
                    <h3 class="h2-xs-custom darkblue-color">Get in Touch with Oneclick Visas</h3><br>
                    <span class="p-md-custom">If we interest you in any service related to immigration, then visit our office to have a cup of coffee with us! We welcome all your queries and doubts. You can avail free counselling sessions with our Immigration experts! You can also reach us through email/phones/mobiles at below given contact information.                  </p>
                </div>
            </div>
            <div class="row locationaddress">
                   <?php include('get.php'); ?>
            </div>
        </div><br>
</section>


<section id="tabs-1" class="bg-lightgrey tabs-section division">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
               <br> <br> <h3 class="h2-xs gray-color">Oneclick has 20,000+ positive reviews</h3>
            </div>
            <center class="col-md-12"><span class="line"></span></center><br>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row brands-holder brand-review-image-smoll">

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid pull-left" src="assets/images/review/google-rev.png" alt="brand-logo" />
                            <div class="pull-left">
                                Google review
                                <ul class="rating-d clearfix staRTSIZE2">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                4.5
                            </div>
                        </div>
                    </div>

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid  pull-left" src="assets/images/review/fb-rev.png" alt="brand-logo" />
                            <div class="pull-left">
                                Google review
                                <ul class="rating-d clearfix staRTSIZE2">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                4.5
                            </div>
                        </div>
                    </div>

                    <!-- BRAND LOGO IMAGE -->
                  
                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid  pull-left" src="assets/images/review/sul-rev.png" alt="brand-logo" />
                            <div class="pull-left">
                                Google review
                                <ul class="rating-d clearfix staRTSIZE2">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                4.5
                            </div>
                        </div>
                    </div>

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid pull-left" src="assets/images/review/trstp-rev.png" alt="brand-logo">
                            <div class="pull-left">
                                Google review
                                <ul class="rating-d clearfix staRTSIZE2">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                4.5
                            </div>
                        </div>
                    </div>

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid" src="assets/images/review/youtb-rev.png" alt="brand-logo" />
                        </div>
                    </div>


                    <div class="col-md-12">
                        <p class="text-center">
                            Onelcick is India’s No.1 Immigration Consultant and World’s largest B2C immigration firm. Established in 1999, our 40+ company owned and managed offices across India, Dubai, Sharjah, Melbourne and Sydney with 1200+ employees serve over 100,000 happy customers per year. We are the best consultant for Canada Work Permit, Federal Skilled Work Visas and other Visas. We will guide you to get the best CRS score, resulting in ITA.
Talk to us today!
                        </p>
                    </div>
                </div> <br>

            </div>
        </div>
    </div>
</section>


<footer id="footer-1" class="bg-blue-map footer division">

    <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <br><br>
        <h3 class="h2-xs-custom white-color text-center">Disclaimer: Oneclick Visas</h3><br>
        <p class="white-color text-center h7font">
            Oneclick Visas is primarily an immigration advisory and trust worthy company that offer consultation for Canada Immigration. Oneclick Visas is based in Delhi NCR and in Canada Montreal at Quebec. Oneclick Visas assist with right and legal information and process the filing for Immigration but we don’t deal in any type of job placement in Canada.
        </p><br>
    </div>
    <div class="col-md-1"></div>
    </div>


    <div class="container white-color">


        <!-- FOOTER CONTENT -->
        <div class="row">


            <!-- FOOTER INFO -->
            <div class="col-lg-4 col-xl-3">
                <div class="footer-info mb-40 media_image-3">
                    <img src="assets/images/iccrc.jpg" width="100%" alt="footer-logo">

                    <img src="assets/images/cac.jpg" width="100%" alt="footer-logo">
                    <img src="assets/images/govt-canada.jpg" width="100%" alt="footer-logo">
                </div>
            </div>


            <!-- FOOTER CONTACTS -->
            <div class="col-lg-3 col-xl-3">
                <div class="footer-box mb-40 headofficefont">
                    <h5 class="h5-md">Office Address</h5>
                    <p>-603, Wave Silver Tower, Noida, Delhi NCR</p>
                    <p>-402, AEON COMPLEX, Navranpura, Ahmedabad</p>
                    <p>2572-Daniel Johnson Boulevard, 2nd Floor, Laval Quebec, H7T 2R3 CANADA</p>

                </div>
            </div>


            <!-- FOOTER LINKS -->
            <div class="col-lg-2 col-xl-3">
                <div class="footer-links mb-40">

                    <!-- Title -->
                    <h5 class="h5-md">Quick Links</h5>

                    <!-- Footer Links -->
                    <ul class="foo-links clearfix">
                        <li><a href="#">About VisaPRO</a></li>
                        <li><a href="#">Our Services</a></li>
                        <li><a href="#">Pricing Packages</a></li>
                        <li><a href="#">Testimonials</a></li>
                        <li><a href="#">From the Blog</a></li>
                    </ul>

                </div>
            </div>


            <!-- FOOTER NEWSLETTER FORM -->
            <div class="col-lg-3 col-xl-3">
                <div class="footer-form mb-20 media_image-6">

                    <!-- Title -->
                    <h5 class="h5-md">Stay Informed</h5>
                    <img src="assets/images/qr-code-300x300.png" width="100%" alt="footer-logo">

                    <div class="textwidget"><p><a href="tel:08448441085"><i class="fa fa-phone"></i>
                        &nbsp;0844-844-1085</a></p> <p>
                            <a href="mailto:enquiry@oneclickvisas.com">
                                <i class="fa fa-envelope-open"></i>&nbsp;
                                Enquiry@oneclickvisas.com</a></p>
                                </div>
                </div>
            </div>

            <div style="width:100%;" style="width: 100%;"></div>
            <div class="font-size14px center-content" style="width:100%;">
                <a href="https://oneclickvisas.com/disclaimer/">Disclaimer</a>
                <a href="https://oneclickvisas.com/terms-and-conditions/">Terms and Conditions</a>
                <a href="https://oneclickvisas.com/privacy-policy-2/">Privacy Policy</a>
                <a href="https://oneclickvisas.com/anti-fraud-policy/">Anti Fraud Policy</a>
                <a href="https://oneclickvisas.com/refund-policy/">Refund Policy</a>
                <a href="https://oneclickvisas.com/our-pricing/">Pricing</a>
                <a href="https://oneclickvisas.com/our-services/">Our Services</a>
                <a href="https://oneclickvisas.com/sitemap.xml">XML Sitemap</a>
            </div>
            <div style="width:100%;" class="font-size14px"> <p>© Copyright <a href="https://oneclickvisas.com/" rel="home">Oneclick Visas</a> 2020. All Rights Reserved</p></div>
            <div style="width:100%;" style="width: 100%; height:20px;"></div>

        </div>
    </div>
</footer>

<div class="desktop-btn white-color hidden-xs">
    <a class="col-md-3 col-xs-6 bottm-btn" href="tel:08448441085"><i class="fa fa-phone"></i>0844-844-1085</a>
    <a class="col-md-3 col-xs-6 bottm-btn" href="skype:info_1041229?"><i class="fa fa-skype" aria-hidden="true"></i> OneclickVisas</a>
    <a class="col-md-3 bottm-btn" href="https://web.whatsapp.com/send?phone=919821533309"><i class="fa fa-whatsapp" aria-hidden="true"></i> WhatsApp +91-9821533309</a>
    <a class="col-md-3 bottm-btn" href="mailto:info@oneclickvisas.com"><i class="fa fa-envelope-open"></i> info@oneclickvisas.com</a>
</div>

<div class="desktop-btn remove-extra-space white-color hidden-md hidden-sm">

    <div class="bottom-footer-fixed1">
        <a href="tel:08448441085"><i class="fa fa-phone"></i>&nbsp; 0844-844-1085</a>
    </div>
    <div class="bottom-footer-fixed2">
        <a class="col-md-12" href="">Enquiry Now</a>
    </div>

</div>

    <div class="enq-btn hidden-xs"><a href="https://oneclickvisas.com/free-canada-assessment-form"><img src="assets/images/enq-vertical.gif" class="img-responsive"></a></div>


</body>

<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/modernizr.custom.js"></script>
<script src="assets/js/jquery.easing.js"></script>
<script src="assets/js/jquery.appear.js"></script>
<script src="assets/js/jquery.stellar.min.js"></script>
<script src="assets/js/menu.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/custom.js"></script>

</html>
