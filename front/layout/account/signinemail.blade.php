 <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-md-offset-3">
                        <div class="account-wall">
                            <h1 class="text-center login-title">Sign in to mail</h1>
                            <form class="form-signin" method="post" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email address" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <button class="btn btn-lg btn-primary btn-block" type="submit">{{ __('Login') }}</button>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                    <div style="width:50%; float:left;" href="#signup" data-toggle="tab">
                                        Create new account
                                    </div>
                                    <div style="width:50%; float:right; text-align: right;" href="#forgotpassword" data-toggle="tab">
                                       Forgot password
                                    </div>
                                    </div>
                                </div>

                            </form>

                            <div class="form-group row" style="padding:30px;">

                                <div class="ortextsetting"><span style="text-align:center;">OR</span><hr></div>

                                <div class="col-md-6">
                                    <img src="{{URL::asset('assets/')}}/images/signinmobile.svg" style="width:100%; margin: auto; display: block;" href="#signinmobile" data-toggle="tab">

                                </div>
                                <div class="col-md-6">
                                    <img src="{{URL::asset('assets/')}}/images/signinemail.svg" style="width:100%; margin: auto; display: block;" href="#signinemail" data-toggle="tab">
                                </div>

                                <div class="col-md-12" style="height:20px;"></div>

                                <div class="col-md-6">
                                    <img src="{{URL::asset('assets/')}}/images/fblogin.svg" style="width:80%; margin: auto; display: block;" href="#sociallogin" data-toggle="tab">
                                </div>

                                <div class="col-md-6">
                                    <img src="{{URL::asset('assets/')}}/images/googlelogin.svg" style="width:80%; margin: auto; display: block;" href="#sociallogin" data-toggle="tab">
                                </div>

                                <div class="col-md-12" style="height:70px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
