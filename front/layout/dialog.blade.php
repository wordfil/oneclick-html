<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class=" ti-close " aria-hidden="true"></span></button>
            </div>
            <div class="modal-body">
                <div class="row no-gutters">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="quickview-slider-active owl-carousel">
                            <a class="img-popup" href="{{URL::asset('assets/')}}/images/product/quickview-1.jpg"><img src="{{URL::asset('assets/')}}/images/product/quickview-elec-1.jpg" alt=""></a>
                            <a class="img-popup" href="{{URL::asset('assets/')}}/images/product/quickview-2.jpg"><img src="{{URL::asset('assets/')}}/images/product/quickview-elec-2.jpg" alt=""></a>
                        </div>
                        <!-- Thumbnail Large Image End -->
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-details-content quickview-content-padding">
                            <h2 class="uppercase">JAW SHARK WOMEN T-SHIRT</h2>
                            <h3>$19.99</h3>
                            <div class="product-details-peragraph">
                                <p>Sed ligula sapien, fermentum id est eget, viverra auctor sem. Vivamus maximus enim vitae urna porta, ut euismod nibh lacinia. Pellentesque at diam sed libero tincidunt feugiat.</p>
                            </div>
                            <div class="product-details-action-wrap">
                                <div class="product-details-quality">
                                    <div class="cart-plus-minus">
                                        <input class="cart-plus-minus-box" type="text" name="qtybutton" value="2">
                                    </div>
                                </div>
                                <div class="product-details-cart">
                                    <a title="Add to cart" href="#">Add to cart</a>
                                </div>
                                <div class="product-details-wishlist">
                                    <a title="Add to wishlist" href="#"><i class="fa fa-heart-o"></i></a>
                                </div>
                                <div class="product-details-compare">
                                    <a title="Add to compare" href="#"><i class="fa fa-compress"></i></a>
                                </div>
                            </div>
                            <div class="product-details-meta">
                                <span>SKU: N/A</span>
                                <span>Categories: <a href="#">Woman</a>, <a href="#">Dress</a>, <a href="#">Style</a>, <a href="#">T-Shirt</a>, <a href="#">Mango</a></span>
                                <span>Tag: <a href="#">Fashion</a>, <a href="#">Dress</a> </span>
                                <span>Product ID: <a href="#">274</a></span>
                            </div>
                            <div class="product-details-info">
                                <a href="#"><i class=" ti-location-pin "></i>Check Store availability</a>
                                <a href="#"><i class=" ti-shopping-cart "></i>Delivery and Return</a>
                                <a href="#"><i class="ti-pin"></i>Ask a Question</a>
                            </div>
                            <div class="product-details-social-wrap">
                                <span>SHARE THIS PRODUCT</span>
                                <div class="product-details-social">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a>
                                    <a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="signinsignup" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class=" ti-close " aria-hidden="true"></span></button>
            </div>
            <div class="modal-body">

                <div class="row no-gutters">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="quickview-slider-active owl-carousel">
                            <a class="img-popup" href="{{URL::asset('assets/')}}/images/product/quickview-1.jpg"><img src="{{URL::asset('assets/')}}/images/product/quickview-elec-1.jpg" alt=""></a>
                            <a class="img-popup" href="{{URL::asset('assets/')}}/images/product/quickview-2.jpg"><img src="{{URL::asset('assets/')}}/images/product/quickview-elec-2.jpg" alt=""></a>
                        </div>
                        <!-- Thumbnail Large Image End -->
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-details-content quickview-content-padding">

                            <div class="product-details-peragraph">











                                <div class="description-review-area section-padding-2 pb-105">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <div class="description-review-wrapper">
                                                    <div class="description-review-topbar nav">
                                                        <a class="active" data-toggle="tab" href="#Login">Login</a>
                                                        <a data-toggle="tab" href="#Registration">Registration</a>
                                                    </div>
                                                    <div class="tab-content description-review-bottom">
                                                        <div id="Login" class="tab-pane active">


{{--                                                            ----------------login------}}

                                                            <form method="POST" action="{{ route('login') }}">
                                                                @csrf
                                                                <div class="form-group row">
                                                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                                        @error('email')
                                                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                                                        @enderror
                                                                </div>

                                                                <div class="form-group row">
                                                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                                                        @error('password')
                                                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                                                        @enderror
                                                                </div>

                                                                <div class="form-group row mb-0">
                                                                    <div class="col-md-12">



                                                                        <div class="product-details-action-wrap">

                                                                            <div class="product-details-cart">
                                                                                <a title="Add to cart" href="#">Add to cart</a>
                                                                            </div>

                                                                        </div>



                                                                        <button type="submit" class="btn btn-primary">
                                                                            {{ __('Login') }}
                                                                        </button>

                                                                        @if (Route::has('password.request'))
                                                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                                                {{ __('Forgot Your Password?') }}
                                                                            </a>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </form>

{{--                                                            ----------------login-close-----}}



                                                        </div>
                                                        <div id="Registration" class="tab-pane">


{{--                                                            1--------------------registration---\--}}

                                                            <form method="POST" action="{{ route('register') }}">
                                                                @csrf

                                                                <div class="form-group row">
                                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                                        @error('name')
                                                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                                                        @error('email')
                                                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                                        @error('password')
                                                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row mb-0">
                                                                    <div class="col-md-6 offset-md-4">
                                                                        <button type="submit" class="btn btn-primary">
                                                                            {{ __('Register') }}
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>

{{--                                                            --------------------registration-close----}}


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>



                            </div>


                            <div class="product-details-info">
                                <a href="#"><i class=" ti-location-pin "></i>Check Store availability</a>
                                <a href="#"><i class=" ti-shopping-cart "></i>Delivery and Return</a>
                                <a href="#"><i class="ti-pin"></i>Ask a Question</a>
                            </div>
                            <div class="product-details-social-wrap">
                                <span>SHARE THIS PRODUCT</span>
                                <div class="product-details-social">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                    <a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a>
                                    <a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



