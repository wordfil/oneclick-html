
<section id="reviews-1" class="bg-lightgrey tabs-section division" style="background:url({{URL::asset('assets/')}}/images/wall.jpg); background-size: cover;">
    <br><br> <div class="container">
            <div class="row">
                <div class="col-md-12 section-title center">
                    <h3 class="h2-xs-custom darkblue-color">Get in Touch with Oneclick Visas</h3><br>
                    <span class="p-md-custom">If we interest you in any service related to immigration, then visit our office to have a cup of coffee with us! We welcome all your queries and doubts. You can avail free counselling sessions with our Immigration experts! You can also reach us through email/phones/mobiles at below given contact information.                  </p>
                </div>
            </div>
            <div class="row locationaddress">
                    @include('front.layout.includes.getintouch')
            </div>
        </div><br>
</section>


<section id="tabs-1" class="bg-lightgrey tabs-section division">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
               <br> <br> <h3 class="h2-xs gray-color">Oneclick has 20,000+ positive reviews</h3>
            </div>
            <center class="col-md-12"><span class="line"></span></center><br>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row brands-holder brand-review-image-smoll">

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid pull-left" src="{{URL::asset('assets/')}}/images/review/google-rev.png" alt="brand-logo" />
                            <div class="pull-left">
                                Google review
                                <ul class="rating-d clearfix staRTSIZE2">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                4.5
                            </div>
                        </div>
                    </div>

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid  pull-left" src="{{URL::asset('assets/')}}/images/review/fb-rev.png" alt="brand-logo" />
                            <div class="pull-left">
                                Google review
                                <ul class="rating-d clearfix staRTSIZE2">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                4.5
                            </div>
                        </div>
                    </div>

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid  pull-left" src="{{URL::asset('assets/')}}/images/review/jd-rev.png" alt="brand-logo" />
                            <div class="pull-left">
                                Google review
                                <ul class="rating-d clearfix staRTSIZE2">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                4.5
                            </div>
                        </div>
                    </div>

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid  pull-left" src="{{URL::asset('assets/')}}/images/review/sul-rev.png" alt="brand-logo" />
                            <div class="pull-left">
                                Google review
                                <ul class="rating-d clearfix staRTSIZE2">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                4.5
                            </div>
                        </div>
                    </div>

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid pull-left" src="{{URL::asset('assets/')}}/images/review/trstp-rev.png" alt="brand-logo">
                            <div class="pull-left">
                                Google review
                                <ul class="rating-d clearfix staRTSIZE2">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                4.5
                            </div>
                        </div>
                    </div>

                    <!-- BRAND LOGO IMAGE -->
                    <div class="col-sm-6 col-md-4">
                        <div class="brand-logo">
                            <img class="img-fluid" src="{{URL::asset('assets/')}}/images/review/youtb-rev.png" alt="brand-logo" />
                        </div>
                    </div>


                    <div class="col-md-12">
                        <p class="text-center">
                            Onelcick is India’s No.1 Immigration Consultant and World’s largest B2C immigration firm. Established in 1999, our 40+ company owned and managed offices across India, Dubai, Sharjah, Melbourne and Sydney with 1200+ employees serve over 100,000 happy customers per year. We are the best consultant for Canada Work Permit, Federal Skilled Work Visas and other Visas. We will guide you to get the best CRS score, resulting in ITA.
Talk to us today!
                        </p>
                    </div>
                </div> <br>

            </div>
        </div>
    </div>
</section>


<footer id="footer-1" class="bg-blue-map footer division">

    <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <br><br>
        <h3 class="h2-xs-custom white-color text-center">Disclaimer: Oneclick Visas</h3><br>
        <p class="white-color text-center h7font">
            Oneclick Visas is primarily an immigration advisory and trust worthy company that offer consultation for Canada Immigration. Oneclick Visas is based in Delhi NCR and in Canada Montreal at Quebec. Oneclick Visas assist with right and legal information and process the filing for Immigration but we don’t deal in any type of job placement in Canada.
        </p><br>
    </div>
    <div class="col-md-1"></div>
    </div>


    <div class="container white-color">


        <!-- FOOTER CONTENT -->
        <div class="row">


            <!-- FOOTER INFO -->
            <div class="col-lg-4 col-xl-3">
                <div class="footer-info mb-40 media_image-3">
                    <img src="{{URL::asset('assets/')}}/images/iccrc.jpg" width="100%" alt="footer-logo">

                    <img src="{{URL::asset('assets/')}}/images/cac.jpg" width="100%" alt="footer-logo">
                    <img src="{{URL::asset('assets/')}}/images/govt-canada.jpg" width="100%" alt="footer-logo">
                </div>
            </div>


            <!-- FOOTER CONTACTS -->
            <div class="col-lg-3 col-xl-3">
                <div class="footer-box mb-40 headofficefont">
                    <h5 class="h5-md">Office Address</h5>
                    <p>-603, Wave Silver Tower, Noida, Delhi NCR</p>
                    <p>-402, AEON COMPLEX, Navranpura, Ahmedabad</p>
                    <p>2572-Daniel Johnson Boulevard, 2nd Floor, Laval Quebec, H7T 2R3 CANADA</p>

                </div>
            </div>


            <!-- FOOTER LINKS -->
            <div class="col-lg-2 col-xl-3">
                <div class="footer-links mb-40">

                    <!-- Title -->
                    <h5 class="h5-md">Quick Links</h5>

                    <!-- Footer Links -->
                    <ul class="foo-links clearfix">
                        <li><a href="#">About VisaPRO</a></li>
                        <li><a href="#">Our Services</a></li>
                        <li><a href="#">Pricing Packages</a></li>
                        <li><a href="#">Testimonials</a></li>
                        <li><a href="#">From the Blog</a></li>
                    </ul>

                </div>
            </div>


            <!-- FOOTER NEWSLETTER FORM -->
            <div class="col-lg-3 col-xl-3">
                <div class="footer-form mb-20 media_image-6">

                    <!-- Title -->
                    <h5 class="h5-md">Stay Informed</h5>
                    <img src="{{URL::asset('assets/')}}/images/qr-code-300x300.png" width="100%" alt="footer-logo">

                    <div class="textwidget"><p><a href="tel:08448441085"><i class="fa fa-phone"></i>
                        &nbsp;0844-844-1085</a></p> <p>
                            <a href="mailto:enquiry@oneclickvisas.com">
                                <i class="fa fa-envelope-open"></i>&nbsp;
                                Enquiry@oneclickvisas.com</a></p>
                                </div>
                </div>
            </div>

            <div style="width:100%;" style="width: 100%;"></div>
            <div class="font-size14px center-content" style="width:100%;">
                <a href="https://oneclickvisas.com/disclaimer/">Disclaimer</a>
                <a href="https://oneclickvisas.com/terms-and-conditions/">Terms and Conditions</a>
                <a href="https://oneclickvisas.com/privacy-policy-2/">Privacy Policy</a>
                <a href="https://oneclickvisas.com/anti-fraud-policy/">Anti Fraud Policy</a>
                <a href="https://oneclickvisas.com/refund-policy/">Refund Policy</a>
                <a href="https://oneclickvisas.com/our-pricing/">Pricing</a>
                <a href="https://oneclickvisas.com/our-services/">Our Services</a>
                <a href="https://oneclickvisas.com/sitemap.xml">XML Sitemap</a>
            </div>
            <div style="width:100%;" class="font-size14px"> <p>© Copyright <a href="https://oneclickvisas.com/" rel="home">Oneclick Visas</a> 2020. All Rights Reserved</p></div>
            <div style="width:100%;" style="width: 100%; height:20px;"></div>

        </div>
    </div>
</footer>

<div class="desktop-btn white-color hidden-xs">
    <a class="col-md-3 col-xs-6 bottm-btn" href="tel:08448441085"><i class="fa fa-phone"></i>0844-844-1085</a>
    <a class="col-md-3 col-xs-6 bottm-btn" href="skype:info_1041229?"><i class="fa fa-skype" aria-hidden="true"></i> OneclickVisas</a>
    <a class="col-md-3 bottm-btn" href="https://web.whatsapp.com/send?phone=919821533309"><i class="fa fa-whatsapp" aria-hidden="true"></i> WhatsApp +91-9821533309</a>
    <a class="col-md-3 bottm-btn" href="mailto:info@oneclickvisas.com"><i class="fa fa-envelope-open"></i> info@oneclickvisas.com</a>
</div>

<div class="desktop-btn remove-extra-space white-color hidden-md hidden-sm">

    <div class="bottom-footer-fixed1">
        <a href="tel:08448441085"><i class="fa fa-phone"></i>&nbsp; 0844-844-1085</a>
    </div>
    <div class="bottom-footer-fixed2">
        <a class="col-md-12" href="">Enquiry Now</a>
    </div>

</div>

    <div class="enq-btn hidden-xs"><a href="https://oneclickvisas.com/free-canada-assessment-form"><img src="{{URL::asset('assets/')}}/images/enq-vertical.gif" class="img-responsive"></a></div>
