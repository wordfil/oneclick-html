<h3 class="ol-md-12 col-lg-12 h3-lg custom-color text-center" style="padding-bottom: 17px;">Why Us? -RCIC Agent    </h3>
<div class="row">


    <!-- TABS NAVIGATION -->
    <div class="col-md-12">
        <div class="tabs-nav clearfix">
            <ul class="tabs-2 primary-tabs">

                <!-- TAB-1 LINK -->
                <li class="tab-link displayed" data-tab="tab-11">
                    <span>Goverment Approved</span>
                </li>

                <!-- TAB-2 LINK -->
                <li class="tab-link" data-tab="tab-12">
                    <span>No Hidden Costs</span>
                </li>

                <!-- TAB-3 LINK -->
                <li class="tab-link" data-tab="tab-13">
                    <span>Fast, Easy & Secure</span>
                </li>

            </ul>
        </div>
    </div>	<!-- END TABS NAVIGATION -->


    <!-- TABS CONTENT -->
    <div class="col-md-12">
        <div class="tabs-content home-tabs">


            <!-- TAB-1 CONTENT -->
            <div id="tab-11" class="tab-content displayed">
                <div class="row d-flex align-items-center">
                    <!-- TAB-1 IMAGE -->
                    <div class="col-md-6 mb-40">
                        <div class="tab-img text-center">
                            <img class="img-fluid" src="{{URL::asset('assets/')}}/images/image-01.jpg" alt="tab-image" />
                        </div>
                    </div>


                    <!-- TAB-1 TEXT -->
                    <div class="col-md-6">
                        <div class="txt-block pc-20 mb-40">
                            <h3 class="h3-lg custom-color">ICCRC Approved Immigration Consultants in India</h3>
                            <ul class="profess-data">
                                <li>
                                    <span>Oneclick Visa is a government-authorised consultancy promising its customers a valuable aid in any type of overseas visa. It is a result of this responsibility and dedication that we have increased our clientele as well as our reputation in providing the most feasible immigration help.</span>
                                </li>
                                <li>
                                    <span>With a regarded name in the business of immigration, we furnish professional help to those interested in immigrating or eventually getting citizenship. Through us, you can relocate to various countries including Canada, New Zealand, Australia and more!</span>
                                </li>
                                <li>
                                    <span>Oneclick provides the quickest immigration and visa help and that too through our certified immigration team members.As you must be aware that Canadian government is cultivating its ties with the Indian government and is looking for skilled immigrants to come and settle in the economy, it is the right time to consider starting with your application process. Contact us for more information.</span>
                                </li>
                                <li>
                                    <span>We provide PR visa in just 6 months!</span>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>  <!-- End row -->
            </div>	<!-- END TAB-1 CONTENT -->


            <!-- TAB-2 CONTENT -->
            <div id="tab-12" class="tab-content">
                <div class="row d-flex align-items-center">


                    <!-- TAB-2 IMAGE -->
                    <div class="col-md-6">
                        <div class="tab-img text-center mb-40">
                            <img class="img-fluid" src="{{URL::asset('assets/')}}/images/image-03.jpg" alt="tab-image" />
                        </div>
                    </div>


                    <!-- TAB-2 TEXT -->
                    <div class="col-md-6">
                        <div class="txt-block pc-20 mb-40">
                            <h3 class="h3-lg custom-color">Get Verified Visa at the Promised Cost! </h3>
                            <ul class="profess-data">
                                <li>
                                    <span>One of the most common tricks in this industry is the hidden cost often charged by the unregulated consultancies. Oneclick Visas, being the ICCRC regulated firm has a fixed fee structure that we mention on our agreement. Not a penny will be charged over the said amount. There are absolutely no concealed or hidden expenses to be incurred later.</span>
                                </li>
                                <li>
                                    <span>As the accredited specialists in the immigration industry, our IRCC experts are fully informed regarding the cost structure of different visas. If in any case, any member of our team is asking you extra money in between your case or after approval of your visa, then straightaway contact our office and we shall take a strict action against such individual.</span>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>  <!-- End row -->
            </div>	<!-- END TAB-2 CONTENT -->


            <!-- TAB-3 CONTENT -->
            <div id="tab-13" class="tab-content">
                <div class="row d-flex align-items-center">


                    <!-- TAB-2 IMAGE -->
                    <div class="col-md-6">
                        <div class="tab-img text-center mb-40">
                            <img class="img-fluid" src="{{URL::asset('assets/')}}/images/image-02.jpg" alt="tab-image" />
                        </div>
                    </div>


                    <!-- TAB-2 TEXT -->
                    <div class="col-md-6">
                        <div class="txt-block pc-20 mb-40">
                            <h3 class="h3-lg custom-color">Approved Visa Within Months!                                        </h3>
                            <ul class="profess-data">
                                <li>
                                    <span>Oneclick being the ICCRC authorised groupis legitimately informedabout laws and policies of different immigrant-host countries. With us, you and your application are completely secure.</span>
                                </li>
                                <li>
                                    <span>As authorised consultants are very much aware of what makes an application get dismissed/ rejected by CIC, their help will permit you to get a PR visa within months without facing any issue during the procedure.</span>
                                </li>
                                <li>
                                    <span>Our team of case officers are trained to manage your application and maintain necessary communications with the immigration authorities at Canada. This ensures easy handling of your profile and quick selection from the pool of candidates!</span>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>  <!-- End row -->
            </div>	<!-- END TAB-2 CONTENT -->


        </div>
    </div>	<!-- END TABS CONTENT -->


</div>
