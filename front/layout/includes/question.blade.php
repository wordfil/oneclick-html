<section id="faqs-1" class="wide-100 faqs-section division">
    <div class="container">


        <!-- SECTION TITLE -->
        <div class="row">
            <div class="col-md-12 section-title center">

                <!-- Title -->
                <h2 class="h2-xs">Have Questions? Look Here</h2>

                <!-- Text -->
                <p class="p-md">Cursus porta, feugiat primis in ultrice ligula risus auctor tempus dolor feugiat,
                    felis lacinia risus interdum auctor id viverra dolor iaculis luctus placerat and massa
                </p>

            </div>
        </div>	 <!-- END SECTION TITLE -->


        <!-- QUESTIONS HOLDER -->
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div id="accordion" role="tablist">


                    <!-- QUESTION #1 -->
                    <div class="card">

                        <!-- Question -->
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="h5-sm">
                                <a data-toggle="collapse" href="#collapseOne" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    <span>1.</span> Which payment methods do you accept?
                                </a>
                            </h5>
                        </div>

                        <!-- Answer -->
                        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">

                                <!-- INFO BOX #1 -->
                                <div class="box-list">
                                    <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                    <p>An magnis nulla dolor sapien augue erat iaculis purus tempor magna ipsum vitae purus
                                        primis pretium ligula rutrum luctus blandit porta justo integer. Feugiat a primis ultrice
                                        ligula risus auctor rhoncus purus ipsum primis
                                    </p>
                                </div>

                                <!-- INFO BOX #2 -->
                                <div class="box-list">
                                    <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                    <p>Nemo ipsam egestas volute turpis dolores ut aliquam quaerat sodales sapien undo pretium
                                        purus ligula tempus ipsum undo auctor a mauris lectus ipsum blandit porta justo integer
                                    </p>
                                </div>

                            </div>
                        </div>


                    </div>	<!-- END QUESTION #1 -->


                    <!-- QUESTION #2 -->
                    <div class="card">

                        <!-- Question -->
                        <div class="card-header" role="tab" id="headingTwo">
                            <h5 class="h5-sm">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="false" aria-controls="collapseTwo">
                                    <span>2.</span> What is the registration process?
                                </a>
                            </h5>
                        </div>

                        <!-- Answer -->
                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">

                                <p>Maecenas gravida porttitor nunc, quis vehicula magna luctus tempor. Quisque vel laoreet
                                    turpis. Urna augue, viverra a augue eget, dictum tempor diam. Sed pulvinar consectetur
                                    nibh, vel imperdiet dui varius viverra. Pellentesque ac massa lorem. Fusce eu cursus est.
                                    Fusce non nulla vitae massa placerat vulputate vel a purus
                                </p>

                            </div>
                        </div>

                    </div>	<!-- END QUESTION #2 -->


                    <!-- QUESTION #3 -->
                    <div class="card">

                        <!-- Question -->
                        <div class="card-header" role="tab" id="headingThree">
                            <h5 class="h5-sm">
                                <a class="collapsed" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">
                                    <span>3.</span> How can I update or cancel my personal information?
                                </a>
                            </h5>
                        </div>

                        <!-- Answer -->
                        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">

                                <!-- Text -->
                                <p>Nullam rutrum eget nunc varius etiam mollis risus congue aliquam etiam sapien egestas,
                                    congue gestas posuere cubilia congue ipsum mauris lectus laoreet gestas neque vitae
                                    auctor eros dolor luctus odio placerat magna cursus
                                </p>

                                <!-- INFO BOX #1 -->
                                <div class="box-list">
                                    <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                    <p>An magnis nulla dolor sapien augue erat iaculis purus tempor magna ipsum vitae purus
                                        primis pretium ligula rutrum luctus blandit porta justo integer. Feugiat a primis ultrice
                                        ligula risus auctor rhoncus purus ipsum primis
                                    </p>
                                </div>

                                <!-- INFO BOX #2 -->
                                <div class="box-list">
                                    <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                    <p>Quaerat sodales sapien undo euismod purus blandit laoreet augue an augue egestas. Augue
                                        iaculis purus tempor congue magna egestas magna ligula rutrum luctus risus ultrice undo
                                        luctus integer congue magna at pretium
                                    </p>
                                </div>

                            </div>
                        </div>

                    </div>	  <!-- END QUESTION #3 -->


                    <!-- QUESTION #4 -->
                    <div class="card">

                        <!-- Question -->
                        <div class="card-header" role="tab" id="headingFour">
                            <h5 class="h5-sm">
                                <a class="collapsed" data-toggle="collapse" href="#collapseFour" role="button" aria-expanded="false" aria-controls="collapseFour">
                                    <span>4.</span> Does your immigration firm offer a money-back guarantee?
                                </a>
                            </h5>
                        </div>

                        <!-- Answer -->
                        <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">

                                <!-- Text -->
                                <p>Curabitur ac dapibus libero. Quisque eu tristique neque. Phasellus blandit tristique justo
                                    ut aliquam. Aliquam vitae molestie nunc. Quisque sapien justo, aliquet non molestie sed purus,
                                    venenatis nec. Aliquam eget lacinia elit. Vestibulum tincidunt tincidunt massa, et porttitor
                                </p>

                            </div>
                        </div>

                    </div>	  <!-- END QUESTION #4 -->


                    <!-- QUESTION #5 -->
                    <div class="card">

                        <!-- Question -->
                        <div class="card-header" role="tab" id="headingFive">
                            <h5 class="h5-sm">
                                <a class="collapsed" data-toggle="collapse" href="#collapseFive" role="button" aria-expanded="false" aria-controls="collapseFive">
                                    <span>5.</span> How long does it take to get a travel visa to Singapore?
                                </a>
                            </h5>
                        </div>

                        <!-- Answer -->
                        <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">

                                <!-- INFO BOX #1 -->
                                <div class="box-list">
                                    <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                    <p>An magnis nulla dolor sapien augue erat iaculis purus tempor magna ipsum vitae purus
                                        primis pretium ligula rutrum luctus blandit porta justo integer. Feugiat a primis ultrice
                                        ligula risus auctor rhoncus purus ipsum primis
                                    </p>
                                </div>

                                <!-- INFO BOX #2 -->
                                <div class="box-list">
                                    <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                    <p>Quaerat sodales sapien undo euismod purus blandit laoreet augue an augue egestas. Augue
                                        iaculis purus tempor congue magna egestas magna ligula rutrum luctus risus ultrice undo
                                        luctus integer
                                    </p>
                                </div>

                            </div>
                        </div>

                    </div>	  <!-- END QUESTION #5 -->


                </div>	<!-- END ACCORDION -->
            </div>  <!-- End col-x -->
        </div>	<!-- END QUESTIONS HOLDER -->


        <!-- MORE QUESTIONS BUTTON -->
        <div class="row">
            <div class="col-md-12 text-center more-questions">
                <h5 class="h5-md">Still have a question? <a href="#" class="darkblue-color">Ask your question here</a></h5>
            </div>
        </div>


    </div>	   <!-- End container -->
</section>	<!-- END FAQs-1 -->

