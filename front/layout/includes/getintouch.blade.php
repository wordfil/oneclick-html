<div class="col-md-3">
    <div>
        <div class="f1"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
        <h4>Our Location</h4>
        <p>-603, Wave Silver Tower, Noida Sector 18, Delhi NCR</p>
        <p>-402, AEON Complex, Navrangpura, Ahmedabad</p>
    </div>
</div>

<div class="col-md-3">
    <div>
        <div class="f1"><i class="fa fa-phone" aria-hidden="true"></i></div>
        <h4>Let’s Talk</h4>
        <p>Phone : +91 98215 333 09</p>
        <p> Phone : +91 98 1874 0875</p>
    </div>
</div>

<div class="col-md-3">
    <div>
        <div class="f1"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
        <h4>Drop a Line </h4>
        <p>info@oneclickvisas.com</p>
        <p>enquiry@oneclickvisas.com</p>
    </div>
</div>

<div class="col-md-3">
    <div>
        <div class="f1"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
        <h4>Working Hours </h4>
        <p>Mon – Fri: 9:30am – 6:30pm</p>
        <p>Sat: 9:30am – 7:30pm</p>
    </div>
</div>
