<section id="services-4" class="bg-lightgrey services-section division">
    <br><br><hr style="border-bottom: 2px gray solid"> <br>
    <div class="container">
        <div class="row ndrowdata">
            <h3 class="ol-md-12 col-lg-12 h3-lg darkblue-color text-center" style="padding-bottom: 17px;">CHECK YOUR ELIGIBILITY</h3>
            <h6 class="h6-md" style="text-align: center; margin: auto;">ONECLICK VISA’S POINT CALCULATOR IS AN EASY WAY TO KNOW YOUR ELIGIBILITY! FILL IN YOUR CREDENTIALS FOR A QUICK ELIGIBILITY OVERVIEW.</h6>
            <br>
            <div class="col-md-4 col-lg-4">
                <h5 class="h5-md canada-flag"><b>Canada PR Visa</b></h5>
            </div>
            <div class="col-md-4 col-lg-4">
                <h5 class="h5-md australia-flag"><b>Canada PR Visa</b></h5>
            </div>
            <div class="col-md-4 col-lg-4">
                <h5 class="h5-md canada-flag"><b>CRS Calculator</b></h5>
            </div>
        </div>
        <br>
    </div>
</section>
