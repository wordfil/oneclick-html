<header id="header-2" class="header white-menu">

    <div class="header-wrapper">

        <div class="rolling-txt">
            <div class="rolling-news">Latest News</div><marquee onmouseover="this.stop();" onmouseout="this.start();" class="rolling-box" style="height: 38px;" scrollamount="4" ;="">
                <p><a href="">Canada welcomes 4,200 skilled workers under new FSWP Express Entry Draw</a> |
                    <a href="">Canada PNP 2020: 5 Easy Steps Canada Provincial Nominee Program</a> |
                    <a href="">Two Ways to Immigrate to Canada in 2020</a> |
                    <a href="">Minimum Income to Sponsor Parents in Canada 2020</a> |
                    <a href="https://oneclickvisas.com/immigration-news/six-reasons-why-your-canadian-pr-visa-may-get-denied/">Six Reasons Why your Canadian Permanent Resident Visa may get denied</a> |  </p>
            </marquee>
        </div>

        <div class="wsmobileheader clearfix">
            <span class="menu-text hidden-md hidden-lg hidden-sm">MENU</span>
            <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
            <span class="smllogo"><img src="{{URL::asset('assets/')}}/images/logo.svg" width="250px" alt="mobile-logo"/></span>
            <a href="tel:123456789" class="callusbtn"><i class="fas fa-phone"></i></a>
        </div>


        <!-- HEADER STRIP -->
        <div class="headtoppart bg-white clearfix">
            <div class="headerwp clearfix">

                <!-- Infotmation -->
                <div class="headertopleft">
                    <div class="header-info clearfix">
                        <span class="txt-400"><i class="fa fa-map-marker" aria-hidden="true"></i> 603, Wave Silver Tower, Noida, Delhi NCR</span>
                    </div>
                </div>

                <!-- Contacts -->
                <div class="headertopright header-contacts">
                    <a href="tel:+918448441085" class="callusbtn txt-400"><i class="fas fa-phone"></i>+91 8448441085</a>
                    <a href="tel:+919821533309" class="callusbtn b-right txt-400">&#8194;+91 9821533309</a>
                    <a href="mailto:info@oneclickvisas.com" class="txt-400"><i class="far fa-envelope-open"></i>info@oneclickvisas.com</a>
                </div>

            </div>
        </div>	<!-- END HEADER STRIP -->


        <!-- NAVIGATION MENU -->
        <div class="wsmainfull menu clearfix">
            <div class="wsmainwp clearfix" style="padding: 0 0px!important;">


                <!-- LOGO IMAGE -->
                <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 360 x 90 pixels) -->
                <div class="desktoplogo"><a href="#hero-3" class="logo-black"><img src="{{URL::asset('assets/')}}/images/logo.svg" width="250px" alt="header-logo"></a></div>
                <div class="desktoplogo"><a href="#hero-3" class="logo-white"><img src="{{URL::asset('assets/')}}/images/logo.svg" width="250px"  alt="header-logo"></a></div>


                <!-- MAIN MENU -->
                <nav class="wsmenu clearfix blue-header">
                    <ul class="wsmenu-list">


                        <li><a href="">Home</a></li>
                        <li aria-haspopup="true"><a href="#">About <span class="wsarrow" style="float: right;
                            margin-left: 42px;"></span></a>
                            <ul class="sub-menu">
                                <li aria-haspopup="true"><a href="#">Why One Click Visas</a></li>
                                <li aria-haspopup="true"><a href="#">Dummy Link #2</a></li>
                                <li aria-haspopup="true"><a href="#">Dummy Link #3</a></li>
                                <li aria-haspopup="true"><a href="#">Dummy Link #4</a></li>
                                <li aria-haspopup="true"><a href="#">Dummy Link #5</a></li>
                            </ul>
                        </li>	<!-- END DROPDOWN MENU -->


                        <!-- PAGES -->
                        <li aria-haspopup="true"><a href="#">Service <span class="wsarrow" style="float: right;
                            margin-left: 42px;"></span></a>
                            <div class="wsmegamenu clearfix">
                                <div class="container">
                                    <div class="row mega-menu-background">




                                        <section id="what-we-do">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="{{URL::asset('assets/')}}/images/flags/canada.png" width="50px" alt="header-logo">Permanent Residency in Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="{{URL::asset('assets/')}}/images/flags/canada.png" width="50px" alt="header-logo">Study in Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="{{URL::asset('assets/')}}/images/flags/canada.png" width="50px" alt="header-logo">Work Permit For Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="{{URL::asset('assets/')}}/images/flags/canada.png" width="50px" alt="header-logo">Tourist Visa for Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="{{URL::asset('assets/')}}/images/flags/canada.png" width="50px" alt="header-logo">Open Work Permit in Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                                        <div class="card">
                                                            <div class="card-block block-1">
                                                                <h5 class="card-title"><img src="{{URL::asset('assets/')}}/images/flags/canada.png" width="50px" alt="header-logo">Spouse Visa for Canada</h5>
                                                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                                <a href="" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>




                                    </div>  <!-- End row -->
                                </div>  <!-- End container -->
                            </div>  <!-- End wsmegamenu -->
                        </li>	<!-- END PAGES -->
                        <li><a href="">News</a></li>
                        <li><a href="">Contact</a></li>
                        <li><a href="">Pay-now</a></li>
                        <li class="nl-simple" aria-haspopup="true">
                            <a href="#" class="header-btn btn-primary tra-black-hover last-link">FREE CONSULTANTS</a>
                        </li>
                        <li><img src="{{URL::asset('assets/')}}/images/IRC.png" class="irc"></li>


                    </ul>
                </nav>	<!-- END MAIN MENU -->

            </div>
        </div>	<!-- END NAVIGATION MENU -->


    </div>     <!-- End header-wrapper -->
</header>	<!-- END HEADER -->
