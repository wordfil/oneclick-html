<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>VisaPRO - Immigration and Visa Consulting Website Template</title>

<link href="{{URL::asset('assets/')}}/compress.css" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">
<link href="{{URL::asset('assets/')}}/css/style.css" rel="stylesheet">
<link href="{{URL::asset('assets/')}}/css/responsive.css" rel="stylesheet">

@stack('custom_css')
