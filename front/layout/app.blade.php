<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
<!-- custom SEO -->
    @stack('custom_seo')
<!-- custom SEO -->
    @include('front.layout.head')

<!-- custom style -->
@stack('custom-style')
<!-- custom style -->
</head>
<body id="background-gradient">
    @include('front.layout.header')
    @include('front.layout.ajax')

    <div id="loginsignupnonew" class="loginsignupnonew">
        <div class="container">
            <div class="col-lg-12 col-sm-12">
                <div>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="signin">
                            @include('front.layout.account.signin')
                        </div>
                        <div class="tab-pane fade in" id="signup">
                            @include('front.layout.account.signup')
                        </div>
                        <div class="tab-pane fade in" id="forgotpassword">
                            @include('front.layout.account.forgot')
                        </div>
                        <div class="tab-pane fade in" id="sociallogin">
                            @include('front.layout.account.social')
                        </div>
                        <div class="tab-pane fade in" id="signinmobile">
                            @include('front.layout.account.signinmobile')
                        </div>
                        <div class="tab-pane fade in" id="signinemail">
                            @include('front.layout.account.signinemail')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content-area-page-all-book">
    @yield('content')
    </div>
    @include('front.layout.footer')
    @include('front.layout.dialog')
    @include('front.layout.script')

    <!-- custom script -->
    @stack('custom-scripts')
</body>
</html>
