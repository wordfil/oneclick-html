<form name="heroRequestForm" class="row hero-request-form bg-darkblue">

    <!-- Request Form Text -->
    <div class="col-md-12 white-color">
        <h5 class="h5-lg">Request Free Consultation</h5>
    </div>

    <!-- Request Form Input -->
    <div id="input-name" class="col-md-12">
        <input type="text" name="name" class="form-control name" placeholder="Enter Your Name*" required>
    </div>

    <!-- Request Form Input -->
    <div id="input-email" class="col-md-12">
        <input type="text" name="email" class="form-control email" placeholder="Enter Your Email*" required>
    </div>

    <!-- Request Form Input -->
    <div id="input-phone" class="col-md-12">
        <input type="tel" name="phone" class="form-control phone" placeholder="Enter Your Phone Number*" required>
    </div>

    <!-- Form Select -->
    <div id="input-visa" class="col-md-12 input-visa">
        <select id="inlineFormCustomSelect2" name="visa" class="custom-select visa" required>
            <option value="">Visa For</option>
            <option>Australia</option>
            <option>Canada</option>
            <option>United Kingdom</option>
            <option>USA</option>
            <option>Singapore</option>
            <option>Netherlands</option>
        </select>
    </div>

    <!-- Request Form Button -->
    <div class="col-md-12 form-btn">
        <button type="submit" class="btn btn-primary tra-white-hover submit">Send Request</button>
    </div>

    <!-- Request Form Message -->
    <div class="col-md-12 hero-form-msg text-center">
        <div class="sending-msg"><span class="loading"></span></div>
    </div>

</form>
