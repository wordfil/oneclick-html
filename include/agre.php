<div class="row brands-holder">

    <!-- BRAND LOGO IMAGE -->
    <div class="col-sm-6 col-md-3">
        <div class="brand-logo">
            <img class="img-fluid" src="assets/images/icons/b1.png" alt="brand-logo" />
        </div>
    </div>

    <!-- BRAND LOGO IMAGE -->
    <div class="col-sm-6 col-md-3">
        <div class="brand-logo">
            <img class="img-fluid" src="assets/images/icons/b2.jpg" alt="brand-logo" />
        </div>
    </div>

    <!-- BRAND LOGO IMAGE -->
    <div class="col-sm-6 col-md-3">
        <div class="brand-logo">
            <img class="img-fluid" src="assets/images/icons/b3.jpg" alt="brand-logo" />
        </div>
    </div>

    <!-- BRAND LOGO IMAGE -->
    <div class="col-sm-6 col-md-3">
        <div class="brand-logo">
            <img class="img-fluid" src="assets/images/icons/b4.jpg" alt="brand-logo" />
        </div>
    </div>

    <!-- BRAND LOGO IMAGE -->
    <div class="col-sm-6 col-md-3">
        <div class="brand-logo">
            <img class="img-fluid" src="assets/images/icons/b5.png" alt="brand-logo">
        </div>
    </div>

    <!-- BRAND LOGO IMAGE -->
    <div class="col-sm-6 col-md-3">
        <div class="brand-logo">
            <img class="img-fluid" src="assets/images/icons/b6.jpg" alt="brand-logo" />
        </div>
    </div>

    <!-- BRAND LOGO IMAGE -->
    <div class="col-sm-6 col-md-3">
        <div class="brand-logo">
            <img class="img-fluid" src="assets/images/icons/b7.jpg" alt="brand-logo" />
        </div>
    </div>

    <!-- BRAND LOGO IMAGE -->
    <div class="col-sm-6 col-md-3">
        <div class="brand-logo">
            <img class="img-fluid" src="assets/images/icons/b8.png" alt="brand-logo" />
        </div>
    </div>


</div>

